package com.janus;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.tinkerpop.gremlin.driver.Result;

import java.util.ArrayList;
import java.util.List;

public class Main{

    public static void main(String args[]) throws Exception{

        XMLConfiguration xmlConfiguration = new XMLConfiguration("conf/graph.xml");
        List<HierarchicalConfiguration> configList = xmlConfiguration.configurationsAt("graph");


        List<String> ipList = new ArrayList<String>();
        ipList.add("127.0.0.1");
        Connector connector = new Connector(ipList);
        connector.connect();
       /* GraphOperator operator = new GraphOperator(connector);
        String v1 = "v1 = graph.addVertex(label,\"File\", \"name\",\"F1\")\n";
        String v2 = "v2 = graph.addVertex(label,\"Message\", \"name\",\"M1\")\n";
        String v3 = "v3 = graph.addVertex(label,\"Message\", \"name\",\"M2\")\n";
        String v4 = "v4 = graph.addVertex(label,\"Instruction\", \"name\",\"I1\")\n";
        String v5 = "v5 = graph.addVertex(label,\"Transaction\", \"name\",\"T1\")\n";
        String e1 = "v1.addEdge(\"File to message\",v2)\n";
        String e2 = "v1.addEdge(\"File to message\",v3)\n";
        String e3 = "v3.addEdge(\"Message to instruction\",v4)\n";
        String e4 = "v4.addEdge(\"Instruction to Transaction\",v5)\n";
        String in = v1+v2+v3+v4+v5+e1+e2+e3+e4;
        connector.submitQuery(in);*/
        StringBuilder builder = new StringBuilder();
        builder.append("m = graph.openManagement()\n");
        builder.append("fileLabel = m.makeVertexLabel(\"File\").make()\n");
        builder.append("messageLabel = m.makeVertexLabel(\"Message\").make()\n");
        builder.append("k1 = m.makePropertyKey(\"customId\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property0\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property1\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property2\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property3\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property4\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property5\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property6\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property7\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property8\").dataType(String.class).make()\n");
        builder.append("k1 = m.makePropertyKey(\"property9\").dataType(String.class).make()\n");
        builder.append("k1 = m.makeEdgeLabel(\"File to message\").make()\n");
        builder.append("m.commit()\n");
        //connector.submitQuery(builder.toString());

       ParamIndexGraphBuilder mbuilder = new ParamIndexGraphBuilder(connector);
       mbuilder.buildGraph();

       /*StringBuilder builder = new StringBuilder();
      /// connector.submitQuery("graph.close()\norg.janusgraph.core.util.JanusGraphCleanup.clear(graph)");
       builder.append("m = graph.openManagement()\n");
       builder.append("k = m.makePropertyKey(\"name\").dataType(String.class).make()\n");
       builder.append("m.buildIndex(\"names\", Vertex.class).addKey(k).unique().buildCompositeIndex()\n");
       builder.append("m.commit()");

       connector.submitQuery(builder.toString());

        String search = "v6 = g.V().hasLabel(\"File\").out(\"File to message\")" +
                ".has(\"name\",\"M2\").out(\"Message to instruction\").has(\"name\",\"I1\").next()\n";
        String v7 = "v7 = graph.addVertex(label,\"Transaction\", \"name\",\"T2\")\n";
        String e5 = "v6.addEdge(\"Instruction to Transaction\",v7)\n";

        List<Result> results = connector.submitQuery(search + v7 + e5);
        System.out.println(results.get(0).getVertex().id());

*/


        //operator.removeAllVertices();
       /* for(HierarchicalConfiguration configuration:configList){
            GraphBuilder graphBuilder = new GraphBuilder(connector,configuration);
            for(int i=0; i < 10000; i++){
                graphBuilder.buildGraph();
            }

        }*/
        //GraphOperator operator = new GraphOperator(connector);
        //operator.removeAllVertices();
        ///operator.getOutVertices("Transaction", "name", "v789887", "Followed by");

    }

}
