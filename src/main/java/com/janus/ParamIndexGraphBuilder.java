package com.janus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParamIndexGraphBuilder {
    int numProperties = 10;
    int fileId = 1;
    private Connector connector;

    public ParamIndexGraphBuilder(Connector connector){

        this.connector = connector;

    }

    public void buildGraph() throws Exception {

        StringBuilder builder = new StringBuilder();
        builder.append("v = graph.addVertex(label,\"File\",\"customId\",\"" + (fileId++) + "\"");
        for(int i=0; i<numProperties; i++){
            builder.append(",\"property"+ i + "\",\"long file property i need" + i + "\"");
        }
        builder.append(")\n");

        builder.append("for(m in messages){\n");
        builder.append("message = graph.addVertex(label,\"Message\",\"customId\",m.customId");
        for(int i=0; i<numProperties; i++){
            builder.append(",\"property"+ i + "\",\"long message property i need" + i + "\"");
        }
        builder.append(")\n");
        builder.append("v.addEdge(\"File to message\",message)\n");
        builder.append("}");

        List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
        Map<String, String> messageMap;
        for(int i = 0; i < 100000; i++){

            messageMap = new HashMap<String, String>();
            messageMap.put("customId","M"+i);
            messageMap.put("a","M"+i);
            mapList.add(messageMap);

        }

        Map<String, Object> params = new HashMap<String , Object>();

        params.put("messages", mapList);

        connector.submitQuery(builder.toString(), params);
    }
}
