package com.janus;

import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ser.GryoMessageSerializerV1d0;
import org.apache.tinkerpop.gremlin.structure.io.gryo.GryoMapper;
import org.janusgraph.graphdb.tinkerpop.JanusGraphIoRegistry;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Connector {

    private List<String> ipList;
    private Client client;
    private Cluster cluster;

    public Connector(List<String> ipList){

        this.ipList = ipList;
    }

    public void connect(){

        Cluster.Builder builder = Cluster.build().maxContentLength(6553600).serializer(new GryoMessageSerializerV1d0(GryoMapper.build().addRegistry(JanusGraphIoRegistry.getInstance())));

        for(String ip:ipList){

            builder = builder.addContactPoint(ip);
        }
        cluster = builder.create();
        client = cluster.connect();
    }

    public List<Result> submitQuery(String query, Map<String, Object> params) throws Exception {
        return client.submit(query, params).all().get();
    }

    public List<Result> submitQuery(String query) throws Exception {
        return client.submit(query).all().get();
    }
}
