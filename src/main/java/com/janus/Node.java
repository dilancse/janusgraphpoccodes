package com.janus;

import java.util.List;
import java.util.Properties;

public class Node {

    private final Vertex vertex;

    private final List<EdgeBean> edgeList;

    public Node(Vertex vertex, List<EdgeBean> edgeList){

        this.vertex = vertex;
        this.edgeList = edgeList;
    }

    public Vertex getVertex() {
        return vertex;
    }

    public static class Vertex{

        private final Properties properties;

        private long vertexId;

        private final String id;
        private final String type;

        public Vertex(String id, String type, Properties properties){

            this.properties = properties;
            this.id = id;
            this.type = type;
        }

        public Properties getProperties() {
            return properties;
        }

        public String getId() {
            return id;
        }

        public String getType() {
            return type;
        }

        public long getVertexId() {
            return vertexId;
        }

        public void setVertexId(long vertexId) {
            this.vertexId = vertexId;
        }
    }

}

