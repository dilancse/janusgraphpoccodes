package com.janus;

import java.util.Properties;

public class EdgeBean {

    private final Properties properties;
    private final String id;
    private final String type;
    private final String from;
    private final String to;

    public EdgeBean(String id, String type, String from, String to, Properties properties){

        this.properties = properties;
        this.id = id;
        this.type = type;
        this.from = from;
        this.to = to;
    }

    public Properties getProperties() {
        return properties;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}