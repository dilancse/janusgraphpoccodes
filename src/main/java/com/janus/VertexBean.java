package com.janus;

import java.util.Properties;

public class VertexBean {

    private final Properties properties;

    private long vertexId;

    private final String id;
    private final String type;

    public VertexBean(String id, String type, Properties properties){

        this.properties = properties;
        this.id = id;
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public long getVertexId() {
        return vertexId;
    }

    public void setVertexId(long vertexId) {
        this.vertexId = vertexId;
    }
}
