package com.janus;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class GraphBuilder {

    public static final AtomicLong graphCounter = new AtomicLong(0);
    public static final Map<String, AtomicLong> propertyValuesMap = new ConcurrentHashMap<String, AtomicLong>();

    private final HierarchicalConfiguration graphConfiguration;
    private final Map<String, VertexBean> vertexBeanMap;
    private final Map<String, EdgeBean> edgeBeanMap;

    private final Connector connector;

    public GraphBuilder(Connector connector, HierarchicalConfiguration graphConfiguration){

        this.graphConfiguration = graphConfiguration;
        vertexBeanMap = new HashMap<String, VertexBean>();
        edgeBeanMap = new HashMap<String, EdgeBean>();
        this.connector = connector;
        extractGraphData();
    }

    private void extractGraphData(){

        List<HierarchicalConfiguration> vertices = graphConfiguration.configurationsAt("vertices.vertex");

        for(HierarchicalConfiguration node:vertices){
            extractVerticesData(node);
        }

        List<HierarchicalConfiguration> edges = graphConfiguration.configurationsAt("edges.edge");

        for(HierarchicalConfiguration edge:edges){
            extractEdgesData(edge);
        }
    }

    private void extractEdgesData(HierarchicalConfiguration edgeConfiguration){

        String edgeId = edgeConfiguration.getString("[@id]");
        String edgeType = edgeConfiguration.getString("[@type]");
        String from = edgeConfiguration.getString("[@from]");
        String to = edgeConfiguration.getString("[@to]");

        Properties properties = extractProperties(edgeConfiguration, "properties.property");

        edgeBeanMap.put(edgeId, new EdgeBean(edgeId, edgeType, from, to, properties));

    }

    private void extractVerticesData(HierarchicalConfiguration verticesConfiguration){

        String id = verticesConfiguration.getString("[@id]");
        String vertexType = verticesConfiguration.getString("[@type]");

        Properties vertexProperties = extractProperties(verticesConfiguration, "properties.property");

        vertexBeanMap.put(id, new VertexBean(id, vertexType, vertexProperties));

    }

    private Properties extractProperties(HierarchicalConfiguration configuration, String xPath){

        Properties properties = new Properties();
        List<HierarchicalConfiguration> propertyConfigurations = configuration.configurationsAt(xPath);
        for(HierarchicalConfiguration propertyConfiguration: propertyConfigurations){

            properties.setProperty(propertyConfiguration.getString("[@name]"),
                    propertyConfiguration.getRootNode().getValue().toString());
        }
        return properties;
    }

    public void buildGraph() throws Exception {

        long graphId = graphCounter.incrementAndGet();
        System.out.println("Creating graph with id " + graphId);
        System.out.println("Creating nodes ...");

        /*for (VertexBean vertexBean :
                vertexBeanMap.values()) {

            StringBuilder builder = new StringBuilder();
            builder.append("graph.addVertex(label,\"").append(vertexBean.getType()).append("\"");

            for(Map.Entry entry :vertexBean.getProperties().entrySet()){

                builder.append(",\"" + entry.getKey() + "\",").append("\"" + entry.getValue() + "\"");
            }
            builder.append(")");

            List<Result> results = connector.submitQuery(builder.toString());
            Vertex vertex = results.get(0).getVertex();
            vertexBean.setVertexId((Long) vertex.id());
            System.out.println("Query executed: " + builder.toString());
            System.out.println("VertexBean added \n \t local id:" + vertexBean.getId() + "\n\t generated id:" + vertex.id());

        }

        System.out.println("Nodes addition completed ...");

        System.out.println("Adding edges ...");

        for (EdgeBean edgeBean :
                edgeBeanMap.values()) {

            StringBuilder builder = new StringBuilder();
            builder.append("g.V(").append(vertexBeanMap.get(edgeBean.getFrom())
                    .getVertexId()).append(").next().addEdge(\"").append(edgeBean.getType()).append("\",").append("g.V(")
                    .append(vertexBeanMap.get(edgeBean.getTo()).getVertexId())
                    .append(").next()").append(")");
            List<Result> results = connector.submitQuery(builder.toString());
            System.out.println();

        }
*/


        Map<String, String> idVsVarNameMap = new HashMap<String, String>();
        int i=1;
        String varName = null;
        StringBuilder builder = new StringBuilder();

        for (VertexBean vertexBean :
                vertexBeanMap.values()) {

            varName = "v" + i++;
            builder.append(varName).append("=graph.addVertex(label,\"").append(vertexBean.getType()).append("\"");
            idVsVarNameMap.put(vertexBean.getId(), varName);

            for(Map.Entry entry :vertexBean.getProperties().entrySet()){

                builder.append(",\"" + entry.getKey() + "\",").append("\"" +  getNextValue("V." + entry.getKey(),entry.getValue().toString()) + "\"");
            }
            builder.append(")\n");
        }

        for (EdgeBean edgeBean :
                edgeBeanMap.values()) {

            builder.append(idVsVarNameMap.get(vertexBeanMap.get(edgeBean.getFrom()).getId())).append(".addEdge(\"")
                    .append(edgeBean.getType()).append("\",").append(idVsVarNameMap.get(vertexBeanMap.get(edgeBean.getTo()).getId()));

            for(Map.Entry entry :edgeBean.getProperties().entrySet()){

                builder.append(",\"" + entry.getKey() + "\",").append("\"" + getNextValue("E." + entry.getKey(),entry.getValue().toString()) + "\"");
            }
                    builder.append(")\n");

        }


        List<Result> results = connector.submitQuery(builder.toString());

        System.out.println("Query executed " + builder.toString());



    }

    private String getNextValue(String key, String value){
        AtomicLong atomicLong = propertyValuesMap.get(key);
        long postFix = 1;
        if(atomicLong == null){
            synchronized (propertyValuesMap){
                if(propertyValuesMap.get(key) == null){
                    propertyValuesMap.put(key, new AtomicLong(1));
                }
            }
        }else{
            postFix = atomicLong.incrementAndGet();
        }

        return value + postFix;
    }


}
