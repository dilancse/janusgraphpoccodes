package com.janus;

import org.apache.tinkerpop.gremlin.driver.Result;

import java.util.List;

public class GraphOperator {
    private final Connector connector;

    public GraphOperator(Connector connector){

        this.connector = connector;
    }

    public void searchForVertex(String label, String paramName, String paramValue) throws Exception {

        StringBuilder builder = new StringBuilder();
        builder.append("g.V().hasLabel(\"").append(label)
                .append("\"").append(").has(\"").append(paramName).append("\",\"").append(paramValue)
                .append("\")");
        List<Result> results = connector.submitQuery(builder.toString());
        System.out.println(results.get(0).getVertex().id());

    }

    public void getOutVertices(String label, String paramName, String paramValue, String edgeType) throws Exception{

        StringBuilder builder = new StringBuilder();
        builder.append("g.V().hasLabel(\"").append(label)
                .append("\"").append(").has(\"").append(paramName).append("\",\"").append(paramValue)
                .append("\").out(\"").append(edgeType).append("\")");
        List<Result> results = connector.submitQuery(builder.toString());
        System.out.println();
    }

    public void removeAllVertices() throws Exception{

        connector.submitQuery("g.E().drop().iterate()");
        connector.submitQuery("g.V().hasLabel(\"Transaction\").drop().iterate()");
        connector.submitQuery("g.V().hasLabel(\"File\").drop().iterate()");
        connector.submitQuery("g.V().hasLabel(\"Message\").drop().iterate()");
        connector.submitQuery("g.V().hasLabel(\"Iteration\").drop().iterate()");
        connector.submitQuery("g.V().hasLabel(\"Block\").drop().iterate()");
    }
}
